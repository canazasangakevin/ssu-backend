"""  routers viewset"""
from rest_framework.routers import DefaultRouter

from apps.users.api.viewsets.user_viewsets import UserViewSet

# Crear un enrutador predeterminado
router = DefaultRouter()

# Registrar el viewset MatriculadoViewSet con el enrutador, y especificar el basename
router.register(r'usuarios', UserViewSet, basename='usuarios')


# Obtener las URLs generadas por el enrutador
urlpatterns = router.urls
