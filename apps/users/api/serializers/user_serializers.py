"""  serializador matriculados """
from rest_framework import serializers
from apps.users.models import User

class UserSerializer(serializers.ModelSerializer):


    class Meta:


        model = User
        fields = '__all__'  # Muestra todos los campos del modelo
