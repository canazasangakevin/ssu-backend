""" MATRICULADOS VIEWSET   """

from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.users.api.serializers.user_serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):

    serializer_class = UserSerializer

    def get_queryset(self, pk=None):
        """
        Obtener el conjunto de consultas para la vista.

        Args:
            pk (str, optional): La clave primaria del objeto a recuperar (por defecto None).

        Returns:
            QuerySet: El conjunto de consultas para la vista.
        """
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        return self.get_serializer().Meta.model.filter(id=pk).first()

    def list(self, request, *args, **kwargs):

        user_serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(user_serializer.data, status=status.HTTP_200_OK)
