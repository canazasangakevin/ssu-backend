# apps/users/utils/user_by_token.py

import jwt


def obtener_usuario_desde_token(token):
    try:
        # Decodificar el token
        decoded_token = jwt.decode(token, options={"verify_signature": False})
        # Obtener el ID del usuario del payload
        user_id = decoded_token.get("user_id")
        return user_id
    except jwt.ExpiredSignatureError:
        # El token ha expirado
        return None
    except jwt.InvalidTokenError:
        # El token es inválido
        return None
