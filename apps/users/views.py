from django.shortcuts import render


# Create your views here.

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from apps.users.utils.user_by_token import obtener_usuario_desde_token
from apps.users.models import User


class ObtenerUsuarioDesdeTokenAPIView(APIView):
    def post(self, request):
        token = request.data.get(
            "token"
        )  # Suponiendo que el token se envía en el cuerpo de la solicitud POST
        if token:
            user_id = obtener_usuario_desde_token(token)
            if user_id:
                # Obtener el objeto del usuario usando el user_id
                user = User.objects.filter(id=user_id).first()
                if user:
                    # Devolver user_id, first_name y last_name del usuario
                    response_data = {
                        "user_id": user.id,
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                    }
                    return Response(response_data, status=status.HTTP_200_OK)
                else:
                    return Response(
                        {"error": "El usuario no fue encontrado."},
                        status=status.HTTP_404_NOT_FOUND,
                    )
            else:
                return Response(
                    {"error": "El token es inválido o ha expirado."},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {"error": "Se requiere un token en la solicitud."},
                status=status.HTTP_400_BAD_REQUEST,
            )
