from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
)
from .managers import CustomUserManager
from simple_history.models import HistoricalRecords



class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(
        "Correo Electrónico",
        max_length=255,
        unique=True,
    )
    first_name = models.CharField("Nombres", max_length=255, blank=True, null=True)
    last_name = models.CharField("Apellidos", max_length=255, blank=True, null=True)
    image = models.ImageField(
        "Imagen de perfil", upload_to="perfil/", max_length=255, null=True, blank=True
    )
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    historical = HistoricalRecords()

    objects = CustomUserManager()

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
        # return f"{self.email}"
