from django.db import models


# Función para definir la ruta de carga de imágenes
def upload_image(instance, filename):
    
    return f"pre_afiliacion/imagenes/{filename}".format(filename=filename)


# Función para definir la ruta de carga de archivos
def upload_file(instance, filename):
    return f"pre_afiliacion/documentos/{filename}".format(filename=filename)


class PreAfiliacion(models.Model):
    class Meta:
        db_table = "pre_afiliacion"

    class Status(models.IntegerChoices):
        NO = 0, "No"
        SI = 1, "Si"

    id_preAfiliacion = models.AutoField(primary_key=True)
    id_estudiante = models.IntegerField()
    id_matricula = models.IntegerField()
    carnet_identidad = models.FileField(upload_to=upload_file, blank=True, null=True)
    matricula_universitaria = models.FileField(upload_to=upload_file, blank=True, null=True)
    certificado_no_afiliacion = models.FileField(upload_to=upload_file, blank=True, null=True)
    certificado_sus = models.FileField(upload_to=upload_file, blank=True, null=True)
    gestora = models.FileField(upload_to=upload_file, blank=True, null=True)
    
    fotografia = models.ImageField(upload_to=upload_image, blank=True, null=True)
    # fotografia = models.ImageField(upload_to='fotos/', blank=True, null=True)
    
    formulario_pre_afiliacion = models.FileField(upload_to=upload_file, blank=True, null=True)
    estado_pre_afiliacion = models.IntegerField(choices=Status.choices, default=Status.SI)
    estado_revision = models.IntegerField(choices=Status.choices, default=Status.NO)
    
    # formulario_pre_afiliacion = models.FileField(
    #     upload_to="documentos/", blank=True, null=True
    # )
    def __str__(self):
        return f"PreAfiliacion {self.id_preAfiliacion}"
