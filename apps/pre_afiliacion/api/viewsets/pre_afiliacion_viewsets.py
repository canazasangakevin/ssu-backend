
from rest_framework import viewsets
from apps.pre_afiliacion.models import PreAfiliacion
from apps.pre_afiliacion.api.serializers.pre_afiliacion_serializer import PreAfiliacionSerializer

class PreAfiliacionViewSet(viewsets.ModelViewSet):
    queryset = PreAfiliacion.objects.all()

    def get_serializer_class(self):
        # Puedes añadir lógica adicional aquí si usas diferentes serializers según la acción
        if self.action in ['create', 'update', 'partial_update']:
            # Aquí puedes retornar diferentes serializers si es necesario
            return PreAfiliacionSerializer
        return PreAfiliacionSerializer