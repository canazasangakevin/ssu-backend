"""  routers viewset"""
from rest_framework.routers import DefaultRouter

from apps.pre_afiliacion.api.viewsets.pre_afiliacion_viewsets import PreAfiliacionViewSet

# Crear un enrutador predeterminado
router = DefaultRouter()

# Registrar el viewset MatriculadoViewSet con el enrutador, y especificar el basename
router.register(r'preafiliados', PreAfiliacionViewSet, basename='preafiliados')

# Obtener las URLs generadas por el enrutador
urlpatterns = router.urls
