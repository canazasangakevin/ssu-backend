"""  serializador pre afiliacion """
from rest_framework import serializers
from apps.pre_afiliacion.models import PreAfiliacion

class PreAfiliacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreAfiliacion
        fields = "__all__" 
        
    