"""  modelo matriculados """

from django.db import models


class Matriculado(models.Model):
    """
    Modelo para acceder a los datos existentes en la vista vista_mae_matriculados_actual.

    Attributes:
        ci (str): Número de carnet de identidad del matriculado (clave primaria).
        nombre (str): Nombre del matriculado.
        paterno (str): Apellido paterno del matriculado.
        materno (str): Apellido materno del matriculado.
        celular (str): Número de celular del matriculado.
        email (str): Dirección de correo electrónico del matriculado.
        registro_universitario (str): Número de registro universitario del matriculado.
        carrera (str): Carrera a la que pertenece el matriculado.
        gestion (str): Gestion a la que pertenece el matriculado.
    """

    class Meta:
        """
        Clase de metadatos para el modelo Matriculado.

        Attributes:
            db_table (str): Nombre de la tabla en la base de datos.
            verbose_name (str): Nombre descriptivo singular del modelo.
            verbose_name_plural (str): Nombre descriptivo plural del modelo.
        """

        db_table = "vista_mae_matriculados_actual" 
    
    ci = models.CharField(max_length=50, primary_key=True)
    id_persona = models.CharField(max_length=20, null=True, blank=True)
    nombre = models.CharField(max_length=150, null=True, blank=True)
    paterno = models.CharField(max_length=150, null=True, blank=True)
    materno = models.CharField(max_length=150, null=True, blank=True)
    celular = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    registro_universitario = models.CharField(max_length=50, null=True, blank=True)
    carrera = models.CharField(max_length=150, null=True, blank=True)
    gestion = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        """
        Devuelve una representación legible en cadena del modelo.

        Returns:
            str: Representación en cadena del modelo.
        """
        return (
            f"CI: {self.ci}, "
            f"Nombre: {self.nombre}, "
            f"Paterno: {self.paterno}, "
            f"Materno: {self.materno}, "
            f"Celular: {self.celular}, "
            f"Email: {self.email}, "
            f"Registro Universitario: {self.registro_universitario}, "
            f"Carrera: {self.carrera}, "
            f"Gestión: {self.gestion}"
        )
