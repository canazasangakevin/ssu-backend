from django.apps import AppConfig

class MatriculadosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.matriculados'
