"""  serializador matriculados """
from rest_framework import serializers
from apps.matriculados.models import Matriculado

class MatriculadoSerializer(serializers.ModelSerializer):
    """
    Serializador para el modelo Matriculado.

    Utiliza el modelo Matriculado para generar la serialización.

    Attributes:
        model (Model): Modelo utilizado para la serialización.
        fields (str): Especifica los campos a incluir en la serialización.
    """

    class Meta:
        """
        Clase de metadatos para el serializador MatriculadoSerializer.

        Attributes:
            model (Model): Modelo utilizado para la serialización.
            fields (str): Especifica los campos a incluir en la serialización.
        """

        model = Matriculado
        fields = '__all__'  # Muestra todos los campos del modelo
