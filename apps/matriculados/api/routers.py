"""  routers viewset"""
from rest_framework.routers import DefaultRouter

from apps.matriculados.api.viewsets.matriculados_viewsets import MatriculadoViewSet

# Crear un enrutador predeterminado
router = DefaultRouter()

# Registrar el viewset MatriculadoViewSet con el enrutador, y especificar el basename
router.register(r'matriculados', MatriculadoViewSet, basename='matriculados')

# Obtener las URLs generadas por el enrutador
urlpatterns = router.urls
