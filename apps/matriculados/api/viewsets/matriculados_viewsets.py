""" MATRICULADOS VIEWSET   """
from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.matriculados.api.serializers.matriculado_serializers import (
    MatriculadoSerializer,
)


class MatriculadoViewSet(viewsets.ModelViewSet):
    """
    ViewSet para manipular datos de Matriculados.

    Este ViewSet proporciona las operaciones CRUD (crear, recuperar, actualizar y eliminar)
    para la entidad Matriculado.

    Attributes:
        serializer_class (Serializer): El serializador utilizado para la entidad Matriculado.
    """

    serializer_class = MatriculadoSerializer

    def get_queryset(self, pk=None):
        """
        Obtener el conjunto de consultas para la vista.

        Args:
            pk (str, optional): La clave primaria del objeto a recuperar (por defecto None).

        Returns:
            QuerySet: El conjunto de consultas para la vista.
        """
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        return self.get_serializer().Meta.model.filter(id=pk).first()

    def list(self, request, *args, **kwargs):
        """
        Obtener una lista de todos los Matriculados.

        Args:
            request (Request): El objeto de solicitud HTTP.
            args (list): Argumentos posicionales adicionales (no utilizados).
            kwargs (dict): Argumentos de palabra clave adicionales (no utilizados).

        Returns:
            Response: La respuesta HTTP con los datos de los Matriculados y el estado HTTP 200 OK.
        """
        matriculado_serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(matriculado_serializer.data, status=status.HTTP_200_OK)
